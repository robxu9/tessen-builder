#!/bin/bash

set -ex

# toolchain dependencies
LC_ALL=C dnf -y install @development-tools @development-libs

# tessbuild dependencies
LC_ALL=C dnf -y install po4a git wget bubblewrap rclone parallel libguestfs \
  moreutils pv ostree jq libuuid-devel \
  gcc-c++ texinfo flex bison glibc-static glibc-devel bsdtar hg buildah \
  libfdisk-devel fuse3-devel grub2-tools-extra xorriso skopeo ninja-build meson \
  qemu virtiofsd rpm-ostree rpm-build bc python3-pyelftools patchelf libgcc cmake fakeroot \
  perl-FindBin
ln -sv bison /usr/bin/yacc
chmod 4755 /usr/bin/sudo
chmod 4755 /usr/bin/newuidmap
chmod 4755 /usr/bin/newgidmap

# we need to fix newuidmap/newgidmap
rpm --restore shadow-utils

# languages we need
LC_ALL=C dnf -y install pipx nodejs nodejs-npm golang

# install pseudo from scratch, sigh
LC_ALL=C dnf -y install acl attr gcc libattr-devel make python3-rpm-macros sqlite-devel
rpmbuild --rebuild /root/pseudo-1.9.0-19.20210205gitf332f56.fc39.src.rpm
LC_ALL=C dnf -y install /root/rpmbuild/RPMS/x86_64/pseudo-1.9.0-*.x86_64.rpm

# fix fakeroot-pseudo :/
sed -i -e 's,UID=,FUID=,' /usr/bin/fakeroot-pseudo
sed -i -e 's,$UID,$FUID,g' /usr/bin/fakeroot-pseudo

# install bootc
LC_ALL=C dnf -y install 'dnf-command(copr)'
LC_ALL=C dnf -y copr enable rhcontainerbot/bootc
LC_ALL=C dnf -y install bootc

# add https://github.com/pengutronix/genimage
LC_ALL=C dnf -y install libconfuse-devel
(
  cd /root
  git clone https://github.com/pengutronix/genimage
  cd genimage
  autoreconf -fiv
  ./configure
  make
  make install
)

# and ldconfig
ldconfig

# set up fuse.conf
echo "user_allow_other" >>/etc/fuse.conf

# set locales
echo "LANG=en_US.UTF-8" >/etc/locale.conf

# add user to group kvm
usermod -a -G kvm user

# get b2
wget -O /usr/local/bin/b2 https://f000.backblazeb2.com/file/backblazefiles/b2/cli/linux/b2 && chmod +x /usr/local/bin/b2

# # set up mise
# curl https://mise.run | sudo -u user bash
# echo 'eval "$(~/.local/bin/mise activate bash)"' | sudo -u user tee -a /home/user/.bashrc
# echo 'PATH="$HOME/.local/share/mise/shims:$PATH"' | sudo -u user tee -a /home/user/.bash_profile

# # install required runtimes
# sudo -i -u user mise use -y -g node@18 go@latest python@latest pipx@latest

# get the aws cli v1; add custom endpoints plugin
sudo -i -u user pipx install awscli
sudo -i -u user pipx inject awscli awscli-plugin-endpoint

# add user bin directories...
sudo -i -u user mkdir -pv /home/user/.local/bin /home/user/bin

# pre-prepare please.build
curl https://get.please.build/get_plz.sh | sudo -i -u user bash
echo 'source <(plz --completion_script)' | sudo -u user tee -a /home/user/.bashrc

# clean dnf
dnf clean all
rm -rf /var/cache/dnf
