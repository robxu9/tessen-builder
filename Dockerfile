FROM fedora:41

RUN useradd -mU user
RUN echo "user ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

COPY pseudo-1.9.0-19.20210205gitf332f56.fc39.src.rpm /root/pseudo-1.9.0-19.20210205gitf332f56.fc39.src.rpm

COPY bootstrap.sh /dsetup/bootstrap.sh
RUN /dsetup/bootstrap.sh

ENV LANG=en_US.UTF-8
USER user
